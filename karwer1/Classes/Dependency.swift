public class Dependency {

    public init() {
        
    }
    
    public func test1() {
        fatalError("This function does not work in 2.0")
    }

    public func test2() {
        print("This function works in 2.0")
    }
    
}
