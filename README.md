# karwer1

[![CI Status](http://img.shields.io/travis/karwer/karwer1.svg?style=flat)](https://travis-ci.org/karwer/karwer1)
[![Version](https://img.shields.io/cocoapods/v/karwer1.svg?style=flat)](http://cocoapods.org/pods/karwer1)
[![License](https://img.shields.io/cocoapods/l/karwer1.svg?style=flat)](http://cocoapods.org/pods/karwer1)
[![Platform](https://img.shields.io/cocoapods/p/karwer1.svg?style=flat)](http://cocoapods.org/pods/karwer1)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

karwer1 is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'karwer1'
```

## Author

karwer, karwer@codequest.com

## License

karwer1 is available under the MIT license. See the LICENSE file for more info.
